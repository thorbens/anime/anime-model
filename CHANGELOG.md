## 1.2.1

* added `UNKNOWN` to rating enumeration

## 1.2.0

* added `Rating` enumeration for audience rating.
* rename of `RatingModel` to `RateModel`
* exposing `IDProviderModel`

## 1.1.0

* Using `string` instead of `Date` for date formats.

## 1.0.0

initial version
