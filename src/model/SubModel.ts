/**
 * Model for a sub.
 */
export interface SubModel {
    /**
     * The name of this subgroup.
     */
    readonly groupName: string;
    /**
     * The status of this sub.
     */
    readonly status: string;
    /**
     * The ISO 8601 last update date.
     */
    readonly lastUpdate: string | null;
    /**
     * The number of subbed episodes.
     */
    readonly episodesCount: number;
    /**
     * The languages which this sub provides.
     */
    readonly languages: string[];
    /**
     * The source for this sub.
     * For an unknown source, this property is null.
     */
    readonly source: string | null;
}
