/**
 * Interface for providing an ID.
 */
export interface IDProviderModel {
    /**
     * The id for this model.
     */
    readonly id: string;
}
