/**
 * Model for a rate.
 */
export interface RateModel {
    /**
     * The average rating.
     * Can be null if no vote is given.
     */
    readonly average: number | null;
    /**
     * The number of votes (total).
     */
    readonly votes: number;
}
