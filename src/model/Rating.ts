/**
 * Rating Audience according to Motion Picture Association of America film rating system.
 *
 * @see https://en.wikipedia.org/wiki/Motion_Picture_Association_of_America_film_rating_system#Ratings
 */
export enum Rating {
    /**
     * G – General Audiences
     * All ages admitted. Nothing that would offend parents for viewing by children.
     */
    G = "G",
    /**
     * PG – Parental Guidance Suggested
     * Some material may not be suitable for children. Parents urged to give "parental guidance". May contain some
     * material parents might not like for their young children.
     */
    PG = "PG",
    /**
     * PG-13 – Parents Strongly Cautioned
     * Some material may be inappropriate for children under 13. Parents are urged to be cautious. Some material may be
     * inappropriate for pre-teenagers.
     */
    PG_13 = "PG-13",
    /**
     * R – Restricted
     * Under 17 requires accompanying parent or adult guardian. Contains some adult material. Parents are urged to
     * learn more about the film before taking their young children with them.
     */
    R = "R",
    /**
     * NC-17 – Adults Only
     * No One 17 and Under Admitted. Clearly adult. Children are not admitted.
     */
    NC_17 = "NC-17",
    /**
     * Unknown ratting.
     */
    UNKNOWN = "unknown",
}
