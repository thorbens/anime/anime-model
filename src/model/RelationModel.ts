/**
 * Model for a relation to another element.
 */
export interface RelationModel<T> {
    /**
     * The type of this relation.
     */
    readonly type: string;
    /**
     * The related id.
     */
    readonly relatedId: string;
    /**
     * The related element.
     */
    readonly relatedElement?: T;
}
