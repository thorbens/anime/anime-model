import {RelationModel} from "./RelationModel";

/**
 * Model for a recommendation to another element.
 */
export interface RecommendationModel<T> extends RelationModel<T> {
    /**
     * The recommendation text.
     */
    readonly recommendationText: string;
    /**
     * The number of recommendations.
     */
    readonly recommendationCount: number;
}
