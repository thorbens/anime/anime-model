import {AiringStatus} from "./AiringStatus";
import {AnimeType} from "./AnimeType";
import {CategoryModel} from "./CategoryModel";
import {EpisodeModel} from "./EpisodeModel";
import {IDProviderModel} from "./IDProviderModel";
import {RateModel} from "./RateModel";
import {Rating} from "./Rating";
import {RecommendationModel} from "./RecommendationModel";
import {RelationModel} from "./RelationModel";
import {ReviewModel} from "./ReviewModel";
import {SubModel} from "./SubModel";

/**
 * Definition of an anime model.
 */
export interface AnimeModel extends IDProviderModel {
    /**
     * The ISO 8601 airing start date of the anime
     * If the airing date is unknown, this property null.
     */
    readonly airingEnd: string | null;
    /**
     * The ISO 8601 airing end date of the anime.
     * If the airing date is unknown, this property null.
     */
    readonly airingStart: string | null;
    /**
     * The airing status of the anime.
     */
    readonly airingStatus: AiringStatus;
    /**
     * The categories set for this anime.
     */
    readonly categories: CategoryModel[];
    /**
     * The url for a cover image.
     */
    readonly coverUrl: string;
    /**
     * An english description.
     */
    readonly descriptionEn: string | null;
    /**
     * The number of episodes for this anime.
     */
    readonly episodeCount: number | null;
    /**
     * The average duration of an episode (in minutes).
     */
    readonly episodeLength: number | null;
    /**
     * The episode for this anime.
     */
    readonly episodes: EpisodeModel[];
    /**
     * The audience Rating for this anime.
     */
    readonly rating: Rating;
    /**
     * The rate for this anime.
     */
    readonly rate: RateModel;
    /**
     * The reviews for this anime.
     */
    readonly reviews: ReviewModel[];
    /**
     * The subs for this anime.
     */
    readonly subs: SubModel[];
    /**
     * A list of synonym for this anime.
     */
    readonly synonyms: string[];
    /**
     * The official (original) title for this anime.
     */
    readonly title: string;
    /**
     * The official english title for this anime.
     */
    readonly titleEn: string | null;
    /**
     * The native, japanese for this anime.
     */
    readonly titleJa: string | null;
    /**
     * The japanese title in romaji (in roman characters).
     */
    readonly titleJaRomaji: string | null;
    /**
     * The type of this anime.
     */
    readonly type: AnimeType;
    /**
     * List of related elements.
     */
    readonly related: Array<RelationModel<AnimeModel>>;
    /**
     * List of recommended elements.
     */
    readonly recommendations: Array<RecommendationModel<AnimeModel>>;
    /**
     * The ISO 8601 date for the last update on this anime.
     */
    readonly lastUpdate: string;
}
