import {IDProviderModel} from "./IDProviderModel";

/**
 * Model for a review.
 */
export interface ReviewModel extends IDProviderModel {
    /**
     * The author name of this review.
     */
    readonly authorName: string;
    /**
     * The url to an avatar.
     */
    readonly avatarUrl: string | null;
    /**
     * The rating of this review.
     */
    readonly rating: number;
    /**
     * The actual review text.
     */
    readonly reviewText: string;
    /**
     * The ISO 8601 review date
     */
    readonly date: string;
    /**
     * The number of people who found this review helpful.
     */
    readonly helpfulCount: number | null;
}
