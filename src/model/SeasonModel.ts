import {AnimeModel} from "./AnimeModel";
import {SeasonName} from "./SeasonName";

/**
 * Model for a season which contains the season year, name and the related animes.
 */
export interface SeasonModel {
    /**
     * The name of this season.
     */
    readonly name: SeasonName;
    /**
     * The year of this season.
     */
    readonly year: number;
    /**
     * The animes in this season.
     */
    readonly animes: AnimeModel[];
}
