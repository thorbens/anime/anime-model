/**
 * Enum for all possible season names.
 */
export enum SeasonName {
    WINTER = "winter",
    SPRING = "spring",
    SUMMER = "summer",
    FALL = "fall",
}
