/**
 * The airing status.
 */
export enum AiringStatus {
    /**
     * Show is currently airing.
     */
    AIRING = "airing",
    /**
     * Airing is completed.
     */
    COMPLETED = "completed",
    /**
     * Will be aired in the future.
     */
    UPCOMING = "upcoming",
    /**
     * Unknown airing status
     */
    UNKNOWN = "unknown",
}
