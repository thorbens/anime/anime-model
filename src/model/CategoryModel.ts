import {IDProviderModel} from "./IDProviderModel";

/**
 * Model for a category.
 */
export interface CategoryModel extends IDProviderModel {
    /**
     * The title of this category.
     */
    readonly title: string;
}
