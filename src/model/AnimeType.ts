/**
 * The detailed type for anime.
 */
export enum AnimeType {
    TV = "tv",
    /**
     * Original Video Animation.
     * ~ are Japanese animated films and series made specially for release
     * in home-video formats without prior showings on television or in theatres,
     * though the first part of an OVA series may be broadcast for promotional purposes.
     * Source: {@link https://en.wikipedia.org/wiki/Original_video_animation}
     *
     */
    OVA = "ova",
    /**
     * Original Net Animation
     * ~ is an anime that is directly released onto the Internet.
     *
     * Source: {@link https://en.wikipedia.org/wiki/Original_video_animation}
     */
    ONA = "ona",
    /**
     * Type for a movie.
     */
    MOVIE = "movie",
    /**
     * Describes web episodes.
     */
    WEB = "web",
    /**
     * Describes special episodes (e.g. for episodes only released with a blu ray).
     */
    SPECIAL = "special",
    /**
     * Use unknown if none of the types matches or the type is unknown for now.
     */
    UNKNOWN = "unknown",
}
