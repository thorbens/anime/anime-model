import {IDProviderModel} from "./IDProviderModel";

/**
 * Model for an episode.
 */
export interface EpisodeModel extends IDProviderModel {
    /**
     * The number of this episode (e.g. 1)
     */
    readonly codeNumber: number;
    /**
     * The original title of this episode.
     */
    readonly title: string | null;
    /**
     * The native, japanese title for this episode
     */
    readonly titleJa: string | null;
    /**
     * The japanese title in romaji (in roman characters).
     */
    readonly titleJaRomaji: string | null;
    /**
     * The preview image url for this episode.
     */
    readonly previewImageUrl: string | null;
    /**
     * If this episode is a filler episode.
     */
    readonly filler: boolean;
    /**
     * If this episode is a recap episode.
     */
    readonly recap: boolean;
}
